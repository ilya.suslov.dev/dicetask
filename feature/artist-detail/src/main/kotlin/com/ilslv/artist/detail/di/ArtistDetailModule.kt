package com.ilslv.artist.detail.di

import com.ilslv.artist.detail.data.ReleaseEntityToReleaseConverter
import com.ilslv.artist.detail.data.ReleasesApi
import com.ilslv.artist.detail.data.ReleasesRepository
import com.ilslv.artist.detail.domain.GetArtistDetailUseCase
import com.ilslv.artist.detail.domain.GetReleasesUseCase
import com.ilslv.artist.search.api.domain.ArtistSearchRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ArtistDetailModule {

    @Singleton
    @Provides
    fun provideReleasesApi(retrofit: Retrofit): ReleasesApi {
        return retrofit.create(ReleasesApi::class.java)
    }

    @Singleton
    @Provides
    fun provideReleaseEntityToReleaseConverter(): ReleaseEntityToReleaseConverter {
        return ReleaseEntityToReleaseConverter()
    }

    @Singleton
    @Provides
    fun provideReleasesRepository(
        releasesApi: ReleasesApi,
        converter: ReleaseEntityToReleaseConverter
    ): ReleasesRepository {
        return ReleasesRepository(releasesApi, converter)
    }

    @Singleton
    @Provides
    fun provideGetArtistDetailUseCase(repository: ArtistSearchRepository): GetArtistDetailUseCase {
        return GetArtistDetailUseCase(repository)
    }

    @Singleton
    @Provides
    fun provideGetReleasesDetailUseCase(repository: ReleasesRepository): GetReleasesUseCase {
        return GetReleasesUseCase(repository)
    }
}