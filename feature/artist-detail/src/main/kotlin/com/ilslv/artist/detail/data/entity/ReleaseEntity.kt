package com.ilslv.artist.detail.data.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ReleasesListBodyWrapper(@Json(name = "release-groups") val releases: List<ReleaseEntity>)

@JsonClass(generateAdapter = true)
class ReleaseEntity(val title: String, val type: String?)