package com.ilslv.artist.detail.domain

import com.ilslv.artist.detail.data.ReleasesRepository
import com.ilslv.artist.detail.domain.model.Release
import com.ilslv.pojoutils.BaseCoroutineUseCase

class GetReleasesUseCase(private val repository: ReleasesRepository) :
    BaseCoroutineUseCase<String, List<Release>>() {

    override suspend fun buildUseCase(params: String): List<Release> {
        return repository.getReleases(params)
    }
}