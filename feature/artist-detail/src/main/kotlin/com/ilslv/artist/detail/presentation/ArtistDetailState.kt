package com.ilslv.artist.detail.presentation

import com.ilslv.artist.detail.domain.model.Release
import com.ilslv.artist.search.api.domain.model.Artist

sealed interface ArtistDetailState {
    data class Success(val artist: Artist) : ArtistDetailState
    data class Error(val message: String) : ArtistDetailState

    object Empty : ArtistDetailState
}

sealed interface ReleasesState {
    object Loading : ReleasesState
    data class Error(val message: String) : ReleasesState

    data class Success(val releases: List<Release>) : ReleasesState
}