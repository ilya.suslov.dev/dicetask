package com.ilslv.artist.detail.data

import com.ilslv.artist.detail.data.entity.ReleasesListBodyWrapper
import retrofit2.http.GET
import retrofit2.http.Query

interface ReleasesApi {

    @GET("release-group")
    suspend fun getAlbums(@Query("artist") artistId: String): ReleasesListBodyWrapper
}