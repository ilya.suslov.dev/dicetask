package com.ilslv.artist.detail.data

import com.ilslv.artist.detail.domain.model.Release

class ReleasesRepository(
    private val releasesApi: ReleasesApi,
    private val converter: ReleaseEntityToReleaseConverter
) {

    suspend fun getReleases(artistId: String): List<Release> {
        val response = releasesApi.getAlbums(artistId)
        return converter.convertList(response.releases)
    }
}