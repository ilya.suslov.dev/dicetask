package com.ilslv.artist.detail.domain

import com.ilslv.artist.search.api.domain.ArtistSearchRepository
import com.ilslv.artist.search.api.domain.model.Artist
import com.ilslv.pojoutils.BaseNonBlockingUseCase

class GetArtistDetailUseCase(
    private val repository: ArtistSearchRepository
) : BaseNonBlockingUseCase<String, Artist>() {

    override fun buildUseCase(params: String): Artist {
        return repository.getArtistById(params)
    }
}