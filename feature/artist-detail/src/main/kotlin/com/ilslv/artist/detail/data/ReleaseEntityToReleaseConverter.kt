package com.ilslv.artist.detail.data

import com.ilslv.artist.detail.data.entity.ReleaseEntity
import com.ilslv.artist.detail.domain.model.Release
import com.ilslv.pojoutils.EntityConverter

class ReleaseEntityToReleaseConverter : EntityConverter<ReleaseEntity, Release>() {

    override fun convert(element: ReleaseEntity): Release {
        return Release(
            title = element.title,
            type = element.type
        )
    }
}