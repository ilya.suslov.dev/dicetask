package com.ilslv.artist.detail.presentation

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ilslv.artist.detail.domain.GetArtistDetailUseCase
import com.ilslv.artist.detail.domain.GetReleasesUseCase
import com.ilslv.core.utils.runSuspendCatching
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArtistDetailViewModel @Inject constructor(
    private val getArtistDetailUseCase: GetArtistDetailUseCase,
    private val getReleasesUseCase: GetReleasesUseCase,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _artistDetailState = MutableStateFlow<ArtistDetailState>(ArtistDetailState.Empty)
    val artistDetailState: StateFlow<ArtistDetailState> = _artistDetailState

    private val _releasesState = MutableStateFlow<ReleasesState>(ReleasesState.Loading)
    val releasesState: StateFlow<ReleasesState> = _releasesState

    init {
        load()
    }

    fun load() {
        viewModelScope.launch {
            // try to find artist by id
            runSuspendCatching {
                val artistId = savedStateHandle.get<String>("artistId").orEmpty()
                val artist = getArtistDetailUseCase.invoke(artistId)
                // show artist data
                _artistDetailState.value =
                    ArtistDetailState.Success(artist = artist)
                // try to find artist releases
                runSuspendCatching {
                    _releasesState.value = ReleasesState.Loading
                    getReleasesUseCase.invoke(artistId)
                }
                    .onFailure {
                        // show error only for the list of releases
                        _releasesState.value = ReleasesState.Error(message = it.message.orEmpty())
                    }
                    .onSuccess {
                        // show releases data
                        _releasesState.value = ReleasesState.Success(releases = it)
                    }
            }
                .onFailure {
                    // show global error
                    _artistDetailState.value = ArtistDetailState.Error(message = it.message.orEmpty())
                }
        }
    }
}