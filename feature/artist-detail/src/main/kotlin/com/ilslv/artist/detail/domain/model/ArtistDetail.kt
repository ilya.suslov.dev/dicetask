package com.ilslv.artist.detail.domain.model

import com.ilslv.artist.search.api.domain.model.Artist

class ArtistDetail(val artist: Artist, val albums: List<Release>)