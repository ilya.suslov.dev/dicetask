package com.ilslv.artist.detail.presentation

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import com.ilslv.artist.detail.domain.model.Release
import com.ilslv.artist.detatil.R
import com.ilslv.core.ui.DiceTaskTheme

@Composable
fun ArtistDetailsScreen(
    viewModel: ArtistDetailViewModel,
    onNavigateBack: () -> Unit = {}
) {
    val state by viewModel.artistDetailState.collectAsState()
    val releasesState by viewModel.releasesState.collectAsState()
    DiceTaskTheme {
        Surface {
            Column(modifier = Modifier.fillMaxSize()) {
                TopAppBar(
                    title = { Text(text = stringResource(id = R.string.artist_detail_artist)) },
                    navigationIcon = {
                        IconButton(onClick = { onNavigateBack.invoke() }) {
                            Icon(
                                imageVector = Icons.Filled.ArrowBack,
                                contentDescription = stringResource(
                                    id = R.string.artist_detail_back
                                )
                            )
                        }
                    }
                )
                Column {
                    when (state) {
                        is ArtistDetailState.Success -> SuccessState(state as ArtistDetailState.Success)
                        is ArtistDetailState.Error -> ErrorState((state as ArtistDetailState.Error).message)
                        is ArtistDetailState.Empty -> Unit
                    }
                    when (releasesState) {
                        is ReleasesState.Loading -> LoadingState()
                        is ReleasesState.Success -> ReleasesList(releases = (releasesState as ReleasesState.Success).releases)
                        is ReleasesState.Error -> ErrorState(message = (releasesState as ReleasesState.Error).message) {
                            viewModel.load()
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun LoadingState() {
    Box(modifier = Modifier.fillMaxSize()) {
        CircularProgressIndicator(
            modifier = Modifier
                .align(Alignment.Center)
                .padding(16.dp)
        )
    }
}

@Composable
private fun ErrorState(message: String, retryAction: (() -> Unit)? = null) {
    Box(modifier = Modifier.fillMaxSize()) {
        Column(modifier = Modifier.align(Alignment.Center)) {
            Text(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                text = stringResource(id = R.string.artist_detail_error_title),
                style = MaterialTheme.typography.h5,
                textAlign = TextAlign.Center
            )
            Text(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                text = message,
                style = MaterialTheme.typography.h5,
                textAlign = TextAlign.Center
            )
            retryAction?.let {
                Button(
                    modifier = Modifier.align(Alignment.CenterHorizontally),
                    onClick = { it.invoke() }
                ) {
                    Text(text = stringResource(id = R.string.artist_detail_retry))
                }
            }
        }

    }
}

@Composable
private fun SuccessState(artistDetail: ArtistDetailState.Success) {
    with(artistDetail) {
        Text(
            modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
            text = artist.name,
            style = MaterialTheme.typography.h2
        )
        artist.type?.let {
            Text(
                modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
                text = it,
                style = MaterialTheme.typography.h5
            )
        }
        artist.city?.let {
            Text(
                modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
                text = it,
                style = MaterialTheme.typography.h5
            )
        }
        artist.country?.let {
            Text(
                modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
                text = it,
                style = MaterialTheme.typography.h5
            )
        }
    }
}

@Composable
private fun ReleasesList(releases: List<Release>) {
    if (releases.isNotEmpty()) {
        Text(
            modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
            text = stringResource(id = R.string.artist_detail_releases),
            style = MaterialTheme.typography.h4,
            textDecoration = TextDecoration.Underline
        )
        LazyColumn {
            items(releases) { release ->
                Column {
                    Text(
                        modifier = Modifier.padding(horizontal = 16.dp, vertical = 4.dp),
                        text = release.title,
                        style = MaterialTheme.typography.h5
                    )
                    release.type?.let {
                        Text(
                            modifier = Modifier.padding(horizontal = 16.dp, vertical = 4.dp),
                            text = it,
                            style = MaterialTheme.typography.caption
                        )
                    }
                    Divider(modifier = Modifier.padding(start = 16.dp))
                }
            }
        }
    }
}