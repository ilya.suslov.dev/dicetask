package com.ilslv.artist.search.test

import com.ilslv.artist.search.api.domain.ArtistSearchRepository
import com.ilslv.artist.search.api.domain.model.Artist
import com.ilslv.artist.search.data.ArtistSearchEntityToArtistConverter
import com.ilslv.artist.search.data.ArtistSearchRepositoryImpl
import com.ilslv.artist.search.data.entity.ArtistSearchEntity
import com.ilslv.artist.search.data.entity.ArtistsListBodyWrapper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class ArtistSearchRepositoryTest {

    private val fakeSearchArtistApi = FakeSearchArtistApi()
    private val repository: ArtistSearchRepository =
        ArtistSearchRepositoryImpl(fakeSearchArtistApi, ArtistSearchEntityToArtistConverter())

    @Test
    fun `fake search api returns list which contains artist with id 1 - getArtistById should return the artist`() =
        runTest {
            val expected = Artist(
                id = "1",
                name = "test1",
                type = null,
                country = null,
                city = null
            )
            repository.search("")
            val actual = repository.getArtistById("1")
            Assert.assertEquals(expected, actual)
        }

    @Test(expected = IllegalStateException::class)
    fun `fake search api returns list which doesn't contain artist with id 1 - getArtistById should throw an exception`() =
        runTest {
            fakeSearchArtistApi.testData = ArtistsListBodyWrapper(
                artists = listOf(
                    ArtistSearchEntity(
                        id = "0",
                        name = "test0",
                        type = null,
                        area = null,
                        beginArea = null
                    ),
                    ArtistSearchEntity(
                        id = "2",
                        name = "test2",
                        type = null,
                        area = null,
                        beginArea = null
                    ),
                    ArtistSearchEntity(
                        id = "3",
                        name = "test3",
                        type = null,
                        area = null,
                        beginArea = null
                    ),
                    ArtistSearchEntity(
                        id = "4",
                        name = "test4",
                        type = null,
                        area = null,
                        beginArea = null
                    )
                )
            )
            repository.search("")
            repository.getArtistById("1")
        }
}