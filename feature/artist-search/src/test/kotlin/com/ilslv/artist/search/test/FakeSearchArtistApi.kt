package com.ilslv.artist.search.test

import com.ilslv.artist.search.data.SearchArtistApi
import com.ilslv.artist.search.data.entity.ArtistSearchEntity
import com.ilslv.artist.search.data.entity.ArtistsListBodyWrapper

class FakeSearchArtistApi : SearchArtistApi {

    var testData = ArtistsListBodyWrapper(
        artists = listOf(
            ArtistSearchEntity(
                id = "0",
                name = "test0",
                type = null,
                area = null,
                beginArea = null
            ),
            ArtistSearchEntity(
                id = "1",
                name = "test1",
                type = null,
                area = null,
                beginArea = null
            ),
            ArtistSearchEntity(
                id = "2",
                name = "test2",
                type = null,
                area = null,
                beginArea = null
            ),
            ArtistSearchEntity(
                id = "3",
                name = "test3",
                type = null,
                area = null,
                beginArea = null
            ),
            ArtistSearchEntity(
                id = "4",
                name = "test4",
                type = null,
                area = null,
                beginArea = null
            )
        )
    )

    override suspend fun search(query: String): ArtistsListBodyWrapper {
        return testData
    }
}