package com.ilslv.artist.search.data.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ArtistsListBodyWrapper(val artists: List<ArtistSearchEntity>)

@JsonClass(generateAdapter = true)
class ArtistSearchEntity(
    val id: String,
    val name: String,
    val type: String?,
    val area: Area?,
    @Json(name = "begin-area") val beginArea: Area?
)

@JsonClass(generateAdapter = true)
class Area(val name: String)