package com.ilslv.artist.search.presentation

import com.ilslv.artist.search.api.domain.model.Artist

sealed interface SearchState {
    data class Error(val message: String) : SearchState
    object Empty : SearchState
    object Loading : SearchState
    data class Success(val artists: List<Artist>) : SearchState
}