package com.ilslv.artist.search.di

import com.ilslv.artist.search.api.domain.ArtistSearchRepository
import com.ilslv.artist.search.data.ArtistSearchEntityToArtistConverter
import com.ilslv.artist.search.data.ArtistSearchRepositoryImpl
import com.ilslv.artist.search.data.SearchArtistApi
import com.ilslv.artist.search.domain.SearchArtistUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class SearchArtistModule {

    @Singleton
    @Provides
    fun provideSearchArtistApi(retrofit: Retrofit): SearchArtistApi {
        return retrofit.create(SearchArtistApi::class.java)
    }

    @Singleton
    @Provides
    fun provideArtistSearchEntityToArtistConverter(): ArtistSearchEntityToArtistConverter {
        return ArtistSearchEntityToArtistConverter()
    }

    @Singleton
    @Provides
    fun provideArtistSearchRepository(
        searchArtistApi: SearchArtistApi,
        converter: ArtistSearchEntityToArtistConverter
    ): ArtistSearchRepository {
        return ArtistSearchRepositoryImpl(searchArtistApi, converter)
    }

    @Singleton
    @Provides
    fun provideSearchArtistUseCase(repository: ArtistSearchRepository): SearchArtistUseCase {
        return SearchArtistUseCase(repository)
    }
}