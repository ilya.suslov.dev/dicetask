package com.ilslv.artist.search.data

import com.ilslv.artist.search.api.domain.model.Artist
import com.ilslv.artist.search.data.entity.ArtistSearchEntity
import com.ilslv.pojoutils.EntityConverter

class ArtistSearchEntityToArtistConverter : EntityConverter<ArtistSearchEntity, Artist>() {

    override fun convert(element: ArtistSearchEntity): Artist {
        return Artist(
            id = element.id,
            type = element.type,
            name = element.name,
            country = element.area?.name,
            city = element.beginArea?.name
        )
    }
}