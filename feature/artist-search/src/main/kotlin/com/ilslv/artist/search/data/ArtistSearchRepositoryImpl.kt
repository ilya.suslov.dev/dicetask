package com.ilslv.artist.search.data

import com.ilslv.artist.search.api.domain.ArtistSearchRepository
import com.ilslv.artist.search.api.domain.model.Artist


class ArtistSearchRepositoryImpl(
    private val searchArtistApi: SearchArtistApi,
    private val converter: ArtistSearchEntityToArtistConverter
) : ArtistSearchRepository {

    private var inMemoryArtistStorage = emptyMap<String, Artist>()

    override suspend fun search(query: String): List<Artist> {
        val response = searchArtistApi.search(query)
        val result = converter.convertList(response.artists)
        inMemoryArtistStorage = result.associateBy { it.id }
        return result
    }

    override fun getArtistById(id: String): Artist {
        return inMemoryArtistStorage[id] ?: throw IllegalStateException("Artist $id is not found")
    }
}