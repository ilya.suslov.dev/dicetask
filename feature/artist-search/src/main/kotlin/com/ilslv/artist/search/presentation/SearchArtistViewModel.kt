package com.ilslv.artist.search.presentation

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ilslv.artist.search.domain.SearchArtistUseCase
import com.ilslv.core.utils.runSuspendCatching
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchArtistViewModel @Inject constructor(private val searchArtistUseCase: SearchArtistUseCase) :
    ViewModel() {

    private val _searchState = MutableStateFlow<SearchState>(SearchState.Empty)
    val searchState: StateFlow<SearchState> = _searchState

    private var searchJob: Job? = null

    fun search(query: String) {
        if (query.length < 3) {
            _searchState.value = SearchState.Empty
            return
        }
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            _searchState.value = SearchState.Loading
            runSuspendCatching {
                searchArtistUseCase.invoke(query)
            }
                .onFailure {
                    Log.e("SearchArtist", it.message.orEmpty())
                    _searchState.value = SearchState.Error(it.localizedMessage.orEmpty())
                }
                .onSuccess {
                    _searchState.value = SearchState.Success(it)
                }
        }
    }

    fun clearSearch() {
        searchJob?.cancel()
        _searchState.value = SearchState.Empty
    }
}