package com.ilslv.artist.search.data

import com.ilslv.artist.search.data.entity.ArtistsListBodyWrapper
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchArtistApi {

    @GET("artist")
    suspend fun search(@Query("query") query: String): ArtistsListBodyWrapper
}