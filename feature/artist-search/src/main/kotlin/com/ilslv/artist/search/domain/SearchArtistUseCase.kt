package com.ilslv.artist.search.domain

import com.ilslv.artist.search.api.domain.ArtistSearchRepository
import com.ilslv.artist.search.api.domain.model.Artist
import com.ilslv.pojoutils.BaseCoroutineUseCase

class SearchArtistUseCase(
    private val repository: ArtistSearchRepository
) : BaseCoroutineUseCase<String, List<Artist>>() {

    override suspend fun buildUseCase(params: String): List<Artist> {
        return repository.search(params)
    }
}