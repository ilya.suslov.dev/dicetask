package com.ilslv.artist.search.api.domain.model

/**
 * Artist model
 * @property id identification of artist on server (used as query params for additional filters)
 * @property type could be Person, Group, Orchestra, Choir, Character, Other (perhaps it'd be better to use Enum, but on UI we need just to render the value without any changes)
 * @property name of Artist
 * @property country of artist
 * @property city of artist
 */
data class Artist(
    val id: String,
    val type: String?,
    val name: String,
    val country: String?,
    val city: String?
)