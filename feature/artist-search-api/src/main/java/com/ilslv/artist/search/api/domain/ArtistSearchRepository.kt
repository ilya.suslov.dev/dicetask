package com.ilslv.artist.search.api.domain

import com.ilslv.artist.search.api.domain.model.Artist

/**
 * Api for searching and getting already downloaded artist info
 */
interface ArtistSearchRepository {
    suspend fun search(query: String): List<Artist>
    fun getArtistById(id: String): Artist
}