# DiceTask

## The app for searching artist and reading information about them

## Tech Stack

- Kotlin
- Coroutines
- Jetpack Compose
- Hilt
- Retrofit
- Moshi
- OkHttp

## Project Structure

The architecture of the project is multimodular with feature and core modules. For the reusing domain
of certain feature modules shall be splited into the API module and the Implementation module.

# Core modules:

- Network
- Pojo Utils
- Utils
- UI

# Feature modules

- Artist Search API
- Artist Search Impl (Called artist-search)
- Artist Details (single module because there is no any reusability of domain of this feature)

# Plugins module

Plugins module contains version catalog integration and reusable Gradle scripts for feature Gradle
modules declaration

## Feature module architecture and structure

Clean architecture and MVVM are base approaches to Feature modules implementation
Every feature module contains data, domain, presentation, and DI modules.
Data contains Retrofit interfaces and the implementation of a repository
Domain contains UseCases
Presentation contains ViewModels and Compose Screens
DI contains only Hilt modules

Some feature modules could have API module. It is some kind of data module from Google developer's
documentation but without any implementation to avoid unnecessary rebuilds at compile time. Instead
of this, every Impl module provides the implementation of the interfaces from API module via Hilt
and the App module builds it into one single dependency graph.

Also, it is an option to split every layer in its own Gradle module

Every Gradle module has a description in build.gradle file. Also, every Core module and API module
has kDoc where it is needed




