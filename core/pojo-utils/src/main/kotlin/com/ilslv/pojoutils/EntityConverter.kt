package com.ilslv.pojoutils

/**
 * Base class for convertors of entities between layers
 * Convenient for tests and complicated conversions
 * Also used for simple conversions for consistency
 */
abstract class EntityConverter<in I, out O> {

    abstract fun convert(element: I): O

    fun convertList(elements: List<I>) = elements.map(::convert)
}