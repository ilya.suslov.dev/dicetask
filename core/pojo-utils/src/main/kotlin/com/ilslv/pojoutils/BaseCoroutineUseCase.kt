package com.ilslv.pojoutils

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Base coroutines use case abstraction for building the use case and for future invocations
 * Default dispatcher is Dispatchers.IO @see dispatcher filed
 * Need to override buildUseCase in all sub-classes
 */
abstract class BaseCoroutineUseCase<in ParamsType, out ReturnType> {

    open val dispatcher: CoroutineDispatcher = Dispatchers.IO

    abstract suspend fun buildUseCase(params: ParamsType): ReturnType

    suspend operator fun invoke(params: ParamsType) = withContext(dispatcher) {
        buildUseCase(params)
    }
}