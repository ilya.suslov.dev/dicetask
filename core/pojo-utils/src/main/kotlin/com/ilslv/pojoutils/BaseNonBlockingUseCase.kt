package com.ilslv.pojoutils

/**
 * Base use case for non blocking operations
 * Need to override buildUseCase in all sub-classes
 */
abstract class BaseNonBlockingUseCase<in ParamsType, out ReturnType> {

    abstract fun buildUseCase(params: ParamsType): ReturnType

    operator fun invoke(params: ParamsType) = buildUseCase(params)
}