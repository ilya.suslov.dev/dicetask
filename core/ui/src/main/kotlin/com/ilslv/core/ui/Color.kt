package com.ilslv.core.ui

import androidx.compose.ui.graphics.Color

val Primary = Color(0xFF375980)
val Secondary = Color(0xFF6DFCD0)