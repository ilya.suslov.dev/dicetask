package com.ilslv.core.utils

import java.util.concurrent.CancellationException

/**
 * runCatching with re-throwing CancellationException because try-catch and default runCatching also catch CancellationException
 * or need to use CoroutineExceptionHandler
 */
suspend fun <R> runSuspendCatching(block: suspend () -> R) = try {
    Result.success(block())
} catch (ex: CancellationException) {
    throw ex
} catch (ex: Exception) {
    Result.failure(ex)
}