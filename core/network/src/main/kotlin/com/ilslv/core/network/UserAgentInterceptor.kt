package com.ilslv.core.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * Interceptor that changes User-Agent from default to anonymous. It's a requirement form documentation about User-Agent value
 */
class UserAgentInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()

        val newRequest: Request = request.newBuilder()
            .removeHeader(HEADER_KEY)
            .addHeader(HEADER_KEY, HEADER_VALUE)
            .build()
        return chain.proceed(newRequest)
    }

    private companion object {
        const val HEADER_KEY = "User-Agent"
        const val HEADER_VALUE = "anonymous"
    }
}