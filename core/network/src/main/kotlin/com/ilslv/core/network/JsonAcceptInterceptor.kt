package com.ilslv.core.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * Interceptor that adds Accept header to request
 * According to documentation it is a requirement to add Accept header application/json to every single request
 */
class JsonAcceptInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()

        val newRequest: Request = request.newBuilder()
            .addHeader(HEADER_KEY, HEADER_VALUE)
            .build()
        return chain.proceed(newRequest)
    }

    private companion object {
        const val HEADER_KEY = "Accept"
        const val HEADER_VALUE = "application/json"
    }
}